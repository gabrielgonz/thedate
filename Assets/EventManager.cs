﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class EventManager : MonoBehaviour {

    private DialogueRunner runner;

    private void Awake()
    {
        runner = FindObjectOfType<DialogueRunner>();
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(StartIntro());
        //StartCoroutine(MessageEvent());
    }

    #region IntroEvent
    IEnumerator StartIntro()
    {
        yield return 0;

        CanvasGroup blockScreen = GameObject.Find("BlockScreen").GetComponent<CanvasGroup>();

        if(runner.variableStorage.GetValue("$IntroEventEnded").AsBool)
        {
            blockScreen.alpha = 0f;
            yield break;
        }

        RectTransform nameGameobject = GameObject.Find("Canvas").transform.Find("NameInputField").GetComponent<RectTransform>();

        MouseHandler.Instance.activated = false;

        yield return new WaitForSeconds(.5f);

        runner.StartDialogue("Intro.Start");

        yield return StartCoroutine(WaitForDialogue());

        Debug.Log("Suena la alarma");

        Debug.Log("Apaga la alarma");

        bool nameEntered = false;

        nameGameobject.gameObject.SetActive(true);
        InputField iField = nameGameobject.GetComponent<InputField>();
        iField.onEndEdit.AddListener((string even) => { nameEntered = true; GameManager.Instance.playerName = even; });

        while (!nameEntered)
        {
            yield return 0;
        }

        nameGameobject.gameObject.SetActive(false);

        runner.StartDialogue("Intro.Part1");

        yield return StartCoroutine(WaitForDialogue());

        nameEntered = false;
        iField.text = "Alicia";
        nameGameobject.gameObject.SetActive(true);
        iField.onEndEdit.AddListener((string even) => { nameEntered = true; GameManager.Instance.femaleName = even; });

        while (!nameEntered)
        {
            yield return 0;
        }

        nameGameobject.gameObject.SetActive(false);

        runner.StartDialogue("Intro.End");

        yield return StartCoroutine(WaitForDialogue());

        yield return StartCoroutine(UnblockScreen(blockScreen));

        MouseHandler.Instance.activated = true;
    }

    IEnumerator UnblockScreen(CanvasGroup blockScreen)
    {
        float currentTime = 0f;
        float theTime = .5f;

        while (currentTime < theTime)
        {
            currentTime += Time.deltaTime;

            float perc = currentTime / theTime;

            blockScreen.alpha = Mathf.Lerp(1f, 0f, perc);

            yield return 0;
        }
    }
    #endregion

    #region MesaggeEvent

    IEnumerator MessageEvent()
    {
        yield return StartCoroutine(WaitForEventEnd());

        yield return StartCoroutine(WaitForDialogue());

        yield return new WaitForSeconds(5f);

        Debug.Log("Suena el movil");

        MouseHandler.Instance.activated = false;

        runner.StartDialogue("MessageEvent.Start");
        
        yield return StartCoroutine(WaitForDialogue());

        Debug.Log("Mostrar imagen de ella");

        runner.StartDialogue("MessageEvent.Part1");

        yield return StartCoroutine(WaitForDialogue());

        runner.StartDialogue("MessageEvent.End");

        yield return StartCoroutine(WaitForDialogue());

        MouseHandler.Instance.activated = true;

        yield return 0;
    }

    #endregion

    #region MiscCode

    IEnumerator WaitForDialogue()
    {
        yield return 0;

        while (runner.isDialogueRunning)
        {
            yield return 0;
        }
    }

    IEnumerator WaitForEventEnd()
    {
        yield return 0;

        bool eventEnded = false;

        while (eventEnded == false)
        {

            if(runner.currentNodeName != null && runner.currentNodeName.Contains(".End"))
            {
                eventEnded = true;
            }

            yield return 0;

        }

    }

    #endregion
}
