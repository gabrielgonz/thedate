﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventStarter : DefaultActivator {

    public GameObject eventToStart;

    public override void DoAction()
    {
        eventToStart.SendMessage("StartEvent");
    }
}
