﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerScript : MonoBehaviour {

    private NavMeshAgent agent;
    private Animator animator;

	// Use this for initialization
	void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

	}

    private void Update()
    {
        if (agent.remainingDistance<=agent.stoppingDistance)
            animator.SetBool("walk", false);
        else
            animator.SetBool("walk", true);
    }
}
