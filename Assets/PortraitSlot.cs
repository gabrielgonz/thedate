﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitSlot : MonoBehaviour {

    public Image icon;

    private Character character;
    private CanvasGroup cGroup;

	// Use this for initialization
	void Start ()
    {
        cGroup = GetComponent<CanvasGroup>();
	}

    public void AddCharacter(Character newChar)
    {
        character = newChar;
        icon.sprite = character.icon;
        cGroup.alpha = 1f;

    }

    public void Clear()
    {
        character = null;
        icon.sprite = null;
        cGroup.alpha = 0f;
    }
}

