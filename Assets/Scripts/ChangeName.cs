﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class ChangeName : MonoBehaviour
{
    public Text currentText;

    [YarnCommand("SetName")]
    public void SetName(string name)
    {
        if (name == "PlayerName")
        {
            currentText.text = GameManager.Instance.playerName;
            return;
        }

        if (name == "FemaleName")
        {
            currentText.text = GameManager.Instance.playerName;
            return;
        }

        currentText.text = name;
    }

}
