﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Character",menuName ="Characters/Character")]
public class Character : ScriptableObject
{
    new public string name = "New Character";
    public string dialogue = "defaultDialogue";
    public House position = House.LivingRoom;
    public Sprite icon = null;

    public virtual void Use()
    {
        Debug.Log("Talking to " + name);
    }


}