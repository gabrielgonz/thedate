﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorScript : DefaultActivator {

    public string sceneToLoad;
    public Transform spawnPoint;
    public CanvasGroup cGroup;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (GameManager.Instance.lastLevel == sceneToLoad)
        {
            Debug.Log("Viene de " + sceneToLoad);
            GameManager.Instance.lastLevel = scene.name;

            //StartCoroutine(ShowScreen());
        }

        
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public override void DoAction()
    {
        MouseHandler.Instance.activated = false;
        StartCoroutine(LoadAsync());    }

    IEnumerator LoadAsync()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneToLoad);
        async.allowSceneActivation = false;

        yield return StartCoroutine(HideScreen());

        yield return new WaitForSeconds(.5f);

        while (async.progress<.9f)
        {
            Debug.Log(async.progress);

            yield return 0;
        }

        async.allowSceneActivation = true;
    }

    IEnumerator ShowScreen()
    {
        float finishTime = 1f;
        float currentTime = 0f;

        while (currentTime != finishTime)
        {
            currentTime += Time.deltaTime;
            if (currentTime > finishTime)
                currentTime = finishTime;

            float perc = currentTime / finishTime;
            cGroup.alpha = Mathf.Lerp(1f, 0f, perc);

            yield return 0;
        }
    }

    IEnumerator HideScreen()
    {
        float finishTime = 1f;
        float currentTime = 0f;

        while (currentTime != finishTime)
        {
            currentTime += Time.deltaTime;
            if (currentTime > finishTime)
                currentTime = finishTime;

            float perc = currentTime / finishTime;
            cGroup.alpha = Mathf.Lerp(0f, 1f, perc);

            yield return 0;
        }
    }
}
