﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;
using UnityEngine.UI;

public class Event_Introduction : MonoBehaviour {

    public RectTransform nameGameobject;
    public CanvasGroup blockScreen;
    private DialogueRunner runner;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(StartIntro());
        runner = FindObjectOfType<DialogueRunner>();
	}

    #region IntroEvent
    IEnumerator StartIntro()
    {
        MouseHandler.Instance.activated = false;

        yield return new WaitForSeconds(.5f);

        runner.StartDialogue("Intro.SetPlayerName");

        yield return 0;

        while (runner.isDialogueRunning)
        {
            yield return 0;
        }

        Debug.Log("Suena la alarma");

        Debug.Log("Apaga la alarma");

        bool nameEntered = false;

        nameGameobject.gameObject.SetActive(true);
        InputField iField = nameGameobject.GetComponent<InputField>();
        iField.onEndEdit.AddListener((string even) => { nameEntered = true; GameManager.Instance.playerName = even; });

        while (!nameEntered)
        {
            yield return 0;
        }

        nameGameobject.gameObject.SetActive(false);

        runner.StartDialogue("Intro.SetFemaleName");

        yield return 0;

        while (runner.isDialogueRunning)
        {
            yield return 0;
        }

        nameEntered = false;
        iField.text = "Alicia";
        nameGameobject.gameObject.SetActive(true);
        iField.onEndEdit.AddListener((string even) => { nameEntered = true; GameManager.Instance.femaleName = even; });

        while (!nameEntered)
        {
            yield return 0;
        }

        nameGameobject.gameObject.SetActive(false);

        runner.StartDialogue("Intro.FadeIn");

        yield return 0;

        while (runner.isDialogueRunning)
        {
            yield return 0;
        }

        yield return StartCoroutine(UnblockScreen());

        MouseHandler.Instance.activated = true;
    }

    IEnumerator UnblockScreen()
    {
        float currentTime = 0f;
        float theTime = .5f;

        while (currentTime<theTime)
        {
            currentTime += Time.deltaTime;

            float perc = currentTime / theTime;

            blockScreen.alpha = Mathf.Lerp(1f, 0f, perc);

            yield return 0;
        }
    }
    #endregion


}
