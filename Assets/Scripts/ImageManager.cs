﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class ImageManager : MonoBehaviour {

    public CanvasGroup currentEmotion;

    private RectTransform charImage;
    private CanvasGroup[] allEmotions;
    


	// Use this for initialization
	void Start ()
    {
        charImage = GetComponent<RectTransform>();
        allEmotions = GetComponentsInChildren<CanvasGroup>();
	}
	
    [YarnCommand("ShowEmotion")]
    public void StartEmotionRoutine(string emotionToShow)
    {
        StartCoroutine(ShowEmotion(emotionToShow));
    }

    IEnumerator ShowEmotion(string emotionToShow)
    {
        CanvasGroup changeEmotion = null;

        foreach (var item in allEmotions)
        {
            if(item.name == emotionToShow)
            {
                changeEmotion = item;
                break;
            }
        }

        float currentTime = 0f;
        float fullTime = .5f;

        while (currentTime<fullTime)
        {
            currentTime += Time.deltaTime;
            if (currentTime > fullTime)
                currentTime = fullTime;

            float perc = currentTime / fullTime;
            currentEmotion.alpha = Mathf.Lerp(1f, 0f, perc);
            changeEmotion.alpha = Mathf.Lerp(0f, 1f, perc);

            yield return 0;

        }

        currentEmotion = changeEmotion;
    }

    IEnumerator MoveImageToFront()
    {
        Vector3 StartPos = new Vector3(350,-160,0);
        Vector3 EndPos = new Vector3(-50, -160, 0);
        float currentTime = 0f;
        float fTime = .5f;

        while (currentTime<fTime)
        {
            currentTime += Time.deltaTime;
            if (currentTime > fTime)
                currentTime = fTime;

            float perc = currentTime / fTime;
            charImage.anchoredPosition3D = Vector3.Lerp(StartPos, EndPos, perc);

            yield return 0;
        }
    }

    IEnumerator MoveImageToBack()
    {
        Vector3 endPos = new Vector3(350, -160, 0);
        Vector3 startPos = new Vector3(-50, -160, 0);
        float currentTime = 0f;
        float fTime = .5f;

        while (currentTime < fTime)
        {
            currentTime += Time.deltaTime;
            if (currentTime > fTime)
                currentTime = fTime;

            float perc = currentTime / fTime;
            charImage.anchoredPosition3D = Vector3.Lerp(startPos, endPos, perc);

            yield return 0;
        }
    }

    [YarnCommand("ShowImage")]
    public void ShowImage()
    {
        StartCoroutine(MoveImageToFront());
    }

    [YarnCommand("HideImage")]
    public void HideImage()
    {
        StartCoroutine(MoveImageToBack());
    }
}
