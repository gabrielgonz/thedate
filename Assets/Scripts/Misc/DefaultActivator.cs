﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultActivator : MonoBehaviour {

	public virtual void DoAction()
    {
        Debug.Log("Se activa el evento de " + transform.name);
    }
}
