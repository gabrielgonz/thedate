﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultEvent : MonoBehaviour {

    public bool eventCanStart = false;
    public bool eventHasEnded = false;

	public virtual void StartEvent()
    {
        Debug.Log("Se inicia el evento en el objecto: " + transform.name);
    }
}
