﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public enum House
{
    LivingRoom,
    BathRoom,
    Room,
    Kitchen
}

public class GameManager : MonoBehaviour {

    
    public string playerName;
    public string femaleName;
    public string lastLevel = string.Empty;
    private DialogueRunner dRunner;


    #region Singleton
    public static GameManager Instance;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    // Update is called once per frame
    void Update ()
    {
          
        if(dRunner== null)
        {
            dRunner = FindObjectOfType<DialogueRunner>();
        }

	}

    #region CharManager
    public List<Character> characters = new List<Character>();
    public void AddCharacter(Character character)
    {
        characters.Add(character);
    }

    public void RemoveCharacter(Character character)
    {
        characters.Remove(character);
    }

    public void SetCharPos(string charName,House position)
    {
        foreach (var item in characters)
        {
            if (item.name == charName)
            {
                item.position = position;
            }
        }
    }

    #endregion

}
