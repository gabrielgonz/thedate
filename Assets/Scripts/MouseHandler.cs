﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class MouseHandler : MonoBehaviour {

    public static MouseHandler Instance { get; private set; }
    public LayerMask mouseMask;
    public bool activated = true;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(MouseChecker());
	}
	

    IEnumerator MouseChecker()
    {

        while (true)
        {
            if (Input.GetMouseButtonDown(0) && activated)
            {
                Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(mouseRay, out hit,100f,mouseMask))
                {
                    hit.collider.SendMessage("DoAction");
                    
                }
            }
            yield return 0;
        }
    }
}
